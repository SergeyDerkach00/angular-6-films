export class FilmInterface {
  public id: number;
  public name: string;
  public year: number;
  public imgUrl: string;
  public description: string;
  public liked?: boolean;
}
