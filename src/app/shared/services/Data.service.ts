import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { catchError, map, tap } from 'rxjs/operators';
import { LocalStorageService } from './../../shared/services/LocalStorageService.service';

// Models
import { FilmInterface } from '../models/film.model';

// Core
import { BaseApi } from '../../core/http/base-api';

// Constants
import { DATE_NOW, DISPLAY_DATE } from '../constants';

// http Options Header
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


/**
 *  API
 * */
// Api filter method URL
const hostName: string = window.location.hostname;
const lang = 'ua';


@Injectable()
export class DataService extends BaseApi {
  // Cookie language
  private storedLang: string;
  private storedLangActive: string;

  constructor(
    private _localSettings: LocalStorageService,
    public http: HttpClient) {
      super(http);
    this.CookieLanguageConstructor();
  }

  configUrl = 'assets/json.json';


  /** Get All Films from the server */
  public getAllFilms(): Observable<FilmInterface[]> {
    return this.get('');
  }


  /**
   * set Likes Local storage
   */
  public setLikes(likeData): void {
    const test = {  like: likeData }​​​​​​​;
    const setLikes = localStorage.setItem('likes', JSON.stringify(test));
  }

  /**
   * get Likes Local storage
   */
  public getLikes(): any {
    const getLikesJSON = localStorage.getItem('likes');
    const getArray = JSON.parse(getLikesJSON);
    return getArray;
    // console.log(getLikes);
  }

  // Set cookie language
  private CookieLanguageConstructor(): any {
    this.storedLangActive = this._localSettings.getLanguage();
    if (this.storedLangActive === '') {
      this._localSettings.setLanguage('ua');
      this.storedLang = this._localSettings.getLanguage();
      // console.warn('DataService if ', this.storedLang);
    } else if (this.storedLangActive === 'en') {
      this.storedLang = 'eng';
      // console.warn('DataService else ', this.storedLang);
    } else {
      this.storedLang = this._localSettings.getLanguage();
    }
  }
}
