import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public links: Array<object>;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    this.links = [
      { path: '/main', label: 'Главная', active: 'button-active', icon: 'home' },
      { path: '/films', label: 'Все фильмы', active: 'button-active', icon: 'list_alt' },
      { path: '/homeWork', label: 'Домашняя работа', active: 'button-active', icon: 'list_alt' }
    ];
  }
  public onLoggedout() {
    localStorage.removeItem('isLoggedin');
  }
  public changeLang(language: string) {
    this.translate.use(language);
  }
}
