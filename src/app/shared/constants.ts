
/** API */
export const PORT = 'api';
const hostName: string = window.location.hostname;

const lang = 'ua';

// Date
export const DATE_NOW = new Date();
export const DISPLAY_DATE = new Date().toLocaleDateString();
