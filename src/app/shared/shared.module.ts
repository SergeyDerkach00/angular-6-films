import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule, FormBuilder, Validators } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppTranslationModule } from '../app.translation.module';

// import { HeaderComponent   } from './components';

// Material
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';


import { SortDirective } from './directives';
import { FilterPipe } from './pipes';
import {   } from './services';

// import {} from './validators';

const NG_MODULES_MATERIAL = [
  MatToolbarModule,  MatIconModule, MatButtonModule, MatCardModule, MatTabsModule,
    MatGridListModule, MatSelectModule, MatSnackBarModule, MatInputModule, MatPaginatorModule
];


const NG_COMPONENTS = [
  // HeaderComponent ,
];

const NG_DIRECTIVES = [
  SortDirective
];
const NG_PIPES = [FilterPipe];
const NG_SERVICES = [];
const NG_VALIDATORS = [

];

@NgModule({
  declarations: [
    ...NG_PIPES,
    ...NG_DIRECTIVES,
    ...NG_COMPONENTS,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AppTranslationModule,
    NgbModule,

    ...NG_MODULES_MATERIAL

  ],
  exports: [
    ...NG_PIPES,
    ...NG_DIRECTIVES,
    ...NG_COMPONENTS,
    ...NG_MODULES_MATERIAL,
    NgbModule,
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: SharedModule,
      providers: [
        ...NG_VALIDATORS,
        ...NG_SERVICES
      ],
    };
  }
}
