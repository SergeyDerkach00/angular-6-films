import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe ({
    name: 'filMoment',
})
export class MomentPipe implements PipeTransform {

    transform(value: string, formatFrom: string, formatTo: 'DD.MM.EEEE'): string {
        return moment(value, formatFrom).format(formatTo);
    }
}
