import { FilmInterface } from './../../../shared/models/film.model';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { FilmService } from './film.service';
import { SearchInterface } from './../../../shared';
import { DataService } from './../../../shared';

import { routerTransition } from '../../../router.animations';

import * as _ from 'lodash';

import { PagerService } from './_services/index';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss'],
  animations: [routerTransition()],
  providers: [
    PagerService
],
})
export class FilmsComponent implements OnInit {
  public filmsData: Array<FilmInterface>;
  public likeCaunt: number;
  public likeListFilms: Array<Object>;

  // form
  public searchValue = '';
  public searchPlaceholder = '';
  public searchField = '';


  // pager
  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];


  constructor(
    protected http: HttpClient,
    protected _dataService: DataService,
    private pagerService: PagerService) {

  }

  ngOnInit(): void {
    this.likeCaunt = 0;
    this.getAllFilms();
    this.likeListFilms = this._dataService.getLikes();

  }

  setUpdatedValue(eventParam) {
    this.likeCaunt = eventParam;
  }

  changeCriteria(field: string) {
    const namesMap = {
      name: 'Имя',
      date: 'Дата',
    };
    this.searchPlaceholder = namesMap[field];
    this.searchField = field;
  }



  private getAllFilms() {
    this._dataService.getAllFilms()
      .subscribe(data => {
        this.filmsData = data;
        this.allItems = data;
        const likesList = this._dataService.getLikes();
        // initialize to page 1
        this.setPage(1);
        if (data) {
          // перебираю массив, Сетаю значение тру или фолс
          this.allItems.forEach(function (elementData) {
            likesList.like.forEach(elementLokal => {
              // console.log('likesList.like', likesList.like);
              if (elementData.id === elementLokal.id) {
                elementData.liked = true;
              } else {
                elementData.liked = false;
              }
            });
          });
        }
      });
  }

  private setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  trackByFn(index, item) {
    return item.id;
  }
}
