import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilmsComponent } from './films.component';
import { FormsModule } from '@angular/forms';
import { DetailsComponent } from './details/details.component';
import { FilmItemComponent } from './film-item/film-item.component';
import { SharedModule } from './../../../shared/shared.module';
import { FilmsRoutingModule } from './films-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    FilmsRoutingModule
  ],
  declarations: [
    FilmsComponent,
    DetailsComponent,
    FilmItemComponent
  ]
})
export class FilmCatalogModule { }
