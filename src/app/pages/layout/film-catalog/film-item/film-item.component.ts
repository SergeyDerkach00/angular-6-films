import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import { FilmInterface,  DataService } from '../../../../shared';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-film-item',
  templateUrl: './film-item.component.html',
  styleUrls: ['./film-item.component.scss']
})
export class FilmItemComponent implements OnInit, AfterViewInit {
  @Input() data: FilmInterface;
  @Input() index: number;
  @Input() filmsData: Array<FilmInterface>;
  @Output() likesCounterOutput: EventEmitter<any> = new EventEmitter();

  public likeIconColor: boolean;

  constructor(
    protected _dataService: DataService,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    const likesList = this._dataService.getLikes();
    this.sendToParent(likesList.like.length);
    // if (this.data.liked === true) {
    //   this.likeIconColor = true;
    // }
  }

  ngAfterViewInit(): void {}

  // Batton clik in item
  public setLike(index): any {
    let filmObject: FilmInterface;

    // Get Array  Local Storage, elements in cliked
    const likesList = this._dataService.getLikes();

    // Set in empty, filmObject cliked Object
    filmObject = this.getFilmObjById(index);

    // Protection against the addition of two similar objects
    if (likesList.like.some(item => item.id === index)) {
      // Delete an object if there is a duplicate.
      likesList.like.some(function (entry, i) {
        if (entry.id === index) {
          likesList.like.splice(i, 1);
          return true;
        }
      });

      filmObject.liked = false;
      console.log('Удалено');
    } else {
      // Set in Local Storage cliked Object
      likesList.like.push(filmObject);
      console.log('Добавлено');
    }

    // Get Array  Local Storage, elements in cliked
    this._dataService.setLikes(likesList.like);
    // Send to parrent Value like.length
    this.sendToParent(likesList.like.length);


    // Alert activate
    this.snackBar.open('Елементов в Избранном ' + likesList.like.length);
  }

  // Send in parent component value many intems in liked
  private sendToParent(setCount): any {
    this.likesCounterOutput.emit(setCount);
  }

  // Get liked Object
  private getFilmObjById(index): FilmInterface {
    const filtered = this.filmsData.filter(item => item.id === index);
    filtered[0].liked = true;
    return filtered[0];
  }
}
