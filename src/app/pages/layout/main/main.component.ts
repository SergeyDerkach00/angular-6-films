import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  animations: [routerTransition()]
})
export class MainComponent implements OnInit {
  public pageName = 'Film Catalog Dashboard';

  list: string[] = ['asd', 'asd'];

  constructor() { }

  ngOnInit() { }

}
