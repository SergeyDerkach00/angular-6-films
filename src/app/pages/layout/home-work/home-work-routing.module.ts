import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeWorkComponent } from './home-work.component';


const routes: Routes = [
    {
        path: '',
        component: HomeWorkComponent,
        children: [
            {path: '', redirectTo: 'TimeModule', pathMatch: 'full'},
            {path: 'TimeModule', loadChildren: './time-module/time.module#TimeModule'},
            {path: 'HelloModule', loadChildren: './hello/hello.module#HelloModule'},
            {path: 'CompIteractions', loadChildren: './components-iteractions/components-iteractions.module#ComponentsIteractionsModule'},
            {path: 'Event', loadChildren: './event/event.module#EventModule'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeWorkRoutingModule {}
