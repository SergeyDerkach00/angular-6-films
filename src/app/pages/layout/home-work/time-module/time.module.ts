import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeComponent } from './time.component';
import { TimeRoutingModule } from './time-routing.module';
import { CountdownComponent } from './countdown-module/countdown.component';
import { MomentModule } from 'ngx-moment';
import { SharedModule } from './../../../../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    TimeRoutingModule,
    MomentModule,
    SharedModule
  ],
  declarations: [
    TimeComponent,
    CountdownComponent]
})
export class TimeModule { }
