import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import 'moment/locale/ru';


const MASSAGE = {
  morning: 'С утра будет завтрак.',
  evening: 'Время близится к ужину.',
  sleep: 'Время близится к сну.',
  night: 'Ночью спать надо, а не кодить.'
};


@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.scss']
})

export class TimeComponent implements OnInit {
  public date: Date = new Date();
  public showMassage: string;
  public nextDay: any;
  constructor() {
  }

  ngOnInit() {
    this.time();
  }

  public time() {
    const hours = this.date.getHours();

    if (hours === 0) { this.showMassage = MASSAGE.night;
    } else if (hours <= 4) { this.showMassage = MASSAGE.morning;
    } else if (hours >= 12) { this.showMassage = MASSAGE.evening;
    } else if (hours >= 12) { this.showMassage = MASSAGE.sleep;
   }

  }

  public timeTo() {
    this.nextDay = new Date(2019, 0, 1);
    return this.nextDay;
  }
}
