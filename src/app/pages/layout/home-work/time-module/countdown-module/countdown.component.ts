import { Component, OnInit } from '@angular/core';
import { TimeComponent } from '../time.component';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent extends TimeComponent implements OnInit {

  constructor() {
    super();

   }

  ngOnInit() {
    this.timeTo();
  }

}
