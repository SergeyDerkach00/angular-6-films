import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountdownComponent } from './countdown.component';
import { CountdownRoutingModule } from './countdown-routing.module';


@NgModule({
  imports: [
    CommonModule,
    CountdownRoutingModule
  ],
  declarations: [CountdownComponent]
})
export class CountdownModule  {
  constructor() {}

}
