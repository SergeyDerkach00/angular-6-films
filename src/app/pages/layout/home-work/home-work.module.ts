import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeWorkRoutingModule } from './home-work-routing.module';

import { HomeWorkComponent } from './home-work.component';
import { HelloComponent } from './hello/hello.component';
import { TimeComponent } from './time-module/time.component';
import { CountdownComponent } from './time-module/countdown-module/countdown.component';
import { MomentModule } from 'ngx-moment';
import { SharedModule } from './..//../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    HomeWorkRoutingModule,
    MomentModule,
    SharedModule
  ],
  declarations: [
      HomeWorkComponent,
    ]
})
export class HomeWorkModule { }
