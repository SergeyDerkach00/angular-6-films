import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsIteractionsComponent } from './components-iteractions.component';


const routes: Routes = [
    {
        path: '',
        component: ComponentsIteractionsComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ComponentsIteractionsRoutingModule {}
