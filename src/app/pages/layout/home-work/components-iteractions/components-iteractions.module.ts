import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsIteractionsComponent } from './components-iteractions.component';
import { ComponentsIteractionsRoutingModule } from './components-iteractions-routing.module';

import { SharedModule } from './../../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ComponentsIteractionsRoutingModule,
    SharedModule
  ],
  declarations: [ComponentsIteractionsComponent]
})
export class ComponentsIteractionsModule { }
