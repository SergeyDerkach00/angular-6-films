import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-components-iteractions',
  templateUrl: './components-iteractions.component.html',
  styleUrls: ['./components-iteractions.component.scss']
})

export class ComponentsIteractionsComponent implements OnInit {
  public helloData: Array<string>;
  public color = true;
  constructor() {

  }

  ngOnInit() {
    this.hello();
  }

  public hello() {
    this.helloData = [
      'Hello World',
      'Привет Мир',
      'Привіт Світ',
      'Hola Mundo',
      'Bonjour le monde', ];
  }

  setColor() {
      this.color = !this.color;
  }
}
