import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.scss']
})

export class HelloComponent implements OnInit {
  public helloData: Array<string>;

  constructor() {

  }

  ngOnInit() {
    this.hello();
  }

  public hello() {
    this.helloData = [
      'Hello World',
      'Привет Мир',
      'Привіт Світ',
      'Hola Mundo',
      'Bonjour le monde', ];
  }

}
