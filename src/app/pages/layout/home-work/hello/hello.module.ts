import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelloComponent } from './hello.component';
import { HelloRoutingModule } from './hello-routing.module';

import { SharedModule } from './../../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    HelloRoutingModule,
    SharedModule
  ],
  declarations: [HelloComponent]
})
export class HelloModule { }
