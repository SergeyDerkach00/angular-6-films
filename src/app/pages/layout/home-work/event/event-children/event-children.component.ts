import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-event-children',
  templateUrl: './event-children.component.html',
  styleUrls: ['./event-children.component.scss']
})

export class EventChildrenComponent implements OnInit {
  @Input() info: string;
  @Output() update = new EventEmitter<string>();
  value: string;

  constructor() {

  }

  ngOnInit() {
    this.value = 'syslik';
  }

  public sendText() {

  }
  setToParent() {
    this.update.emit(this.value);
  }

}
