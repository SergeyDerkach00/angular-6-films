import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventComponent } from './event.component';
import { EventRoutingModule } from './event-routing.module';

import { EventChildrenComponent } from './event-children/event-children.component';
import { SharedModule } from './../../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    EventRoutingModule,
    SharedModule
  ],
  declarations: [EventComponent, EventChildrenComponent]
})
export class EventModule { }
