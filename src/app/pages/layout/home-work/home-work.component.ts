import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-home-work',
  templateUrl: './home-work.component.html',
  styleUrls: ['./home-work.component.scss'],
  animations: [routerTransition()]
})
export class HomeWorkComponent implements OnInit {
  links: object[] = [
    { path: '/homeWork/TimeModule', label: 'TimeModule'},
    { path: '/homeWork/HelloModule', label: 'HelloModule' },
    { path: '/homeWork/CompIteractions', label: 'ComponentsIteractions'},
    { path: '/homeWork/Event', label: 'Event'}
  ];

  constructor(public router: Router) { }

  ngOnInit() {
  }

}
