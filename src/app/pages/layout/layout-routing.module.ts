import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [

            { path: '', pathMatch: 'full', redirectTo: 'main' },
            { path: 'main', loadChildren: './main/main.module#MainModule' },
            { path: 'films', loadChildren: './film-catalog/films.module#FilmCatalogModule' },
            { path: 'homeWork', loadChildren: './home-work/home-work.module#HomeWorkModule'},

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
