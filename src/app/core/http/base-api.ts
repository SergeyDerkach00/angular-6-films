import { Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
@Injectable()
export class BaseApi {
  // private baseUrl = 'http://localhost:3000/';
  private baseUrl = 'assets/json.json';
  // configUrl = 'assets/json.json';
  constructor(public http: HttpClient) {
  }


  private getUrl(url: string = ''): string {
    return this.baseUrl + url;
  }

  public get(url: string = ''): Observable<any> {
    return this.http.get(this.getUrl(url))
    .pipe(
      tap(response => {
        return response;
      })
    );
  }

  public post(url: string = '', data: any = {}): Observable<any> {
    return this.http.post(this.getUrl(url), data)
    .pipe(
      tap(response => {
        return response;
      })
    );
  }

  public put(url: string = '', data: any = {}): Observable<any> {
    return this.http.put(this.getUrl(url), data)
    .pipe(
      tap(response => {
        return response;
      })
    );
  }
}
