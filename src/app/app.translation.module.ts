import { NgModule } from '@angular/core';
// import 'rxjs/add/observable/throw';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/toPromise';
import { Observable, of,  } from 'rxjs';
import { catchError, map, tap,  } from 'rxjs/operators';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateService } from '@ngx-translate/core';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

import { LocalSettingsService } from './shared/services/LocalSettingsService.service';
const translationOptions = {
  loader: {
    provide: TranslateLoader,
    useFactory: HttpLoaderFactory,
    deps: [HttpClient]
  }
};

@NgModule({
  imports: [HttpClientModule,
    TranslateModule.forRoot(translationOptions)],
  exports: [TranslateModule],
  providers: [TranslateService]
})

export class AppTranslationModule {
  storedLang: string;
  constructor(private translate: TranslateService,
    _localSettings: LocalSettingsService,
  ) {

    translate.addLangs(['ua']);
    this.storedLang = _localSettings.getLanguage();
    // translate.setDefaultLang('ua');

    translate.setDefaultLang(this.storedLang);
    // console.log('AppTranslationModule', this.storedLang);
    translate.addLangs(['en', 'ru']);
    // translate.use('en');
  }
}
